FLICKR.services = {}

FLICKR.services.getRecent = function(options) {
	FLICKR.params.method = "flickr.photos.getRecent";
	FLICKR.post();
};

FLICKR.services.subscribe = function(options) {
	FLICKR.params.method = "flickr.push.subscribe";
	FLICKR.post();
};

FLICKR.services.search = function() {
	FLICKR.params.method = "flickr.photos.search";

	
	var milliseconds = (new Date).getTime();

	milliseconds = Math.ceil(milliseconds / 1000);

	//console.log("Posting at " + milliseconds);

	var min = milliseconds - 1200;
	var max = milliseconds;
	
	console.log("From " + new Date(min) + " to " + new Date(max));

	var options = {
		"min_upload_date" : min,
		"max_upload_date" : max,
		"has_geo" : "1",
		"extras" : "geo, url_z, last_update",
		"page" : "1",
		"sort" : "date-posted-asc"
	};

	var $holder = $("#photos");

	function searchCallback(result) {
		var photos = result.photos;

		$.each(photos.photo, function(i, photo) {
			//photos.push(photo);
			QUEUE.photos.push(photo);
		});
		
		
		
		//console.log(QUEUE.photos.length);
		
		if (QUEUE.photos.length > 0) {
			revealScroller();
//			revealAll();
		}
		
		//console.log(photos.pages + " --- " + photos.page);
		
		if (photos.pages > photos.page) {
			options.page = photos.page + 1;
			FLICKR.post(options, searchCallback);
			
		}
	}

	FLICKR.post(options, searchCallback);
};

