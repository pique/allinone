MAPS = {};
MAPS.map = null;

MAPS.overlay = new google.maps.OverlayView();

MAPS.initialize = function initialize() {
	var mapOptions = {
		zoom : 2,
		center : new google.maps.LatLng(13.239945,6.679688),
		disableDefaultUI : true,
		mapTypeId : google.maps.MapTypeId.ROADMAP,

		draggable : false,
		zoomControl : false,
		scrollwheel : false,
		disableDoubleClickZoom : true
	};

	var styles = [{
		featureType : "all",
		elementType : "geometry.fill",
		stylers : [{
			color : "#680000"
		}, {
			visibility : "on"
		}]
	}, {
		featureType : "administrative",
		elementType : "geometry",
		stylers : [{
			hue : "#ff0000"
		}, {
			visibility : "off"
		}]
	}, {
		featureType : "administrative.country",
		elementType : "geometry.stroke",
		stylers : [{
			color : "#cccc00"
		}, {
			visibility : "off"
		}]
	}, {
		featureType : "water",
		elementType : "geometry.fill",
		stylers : [{
			color : "#000000"
		}, {
			visibility : "on"
		}]
	}, {
		featureType : "water",
		elementType : "labels",
		stylers : [{
			color : "#cccccc"
		}, {
			visibility : "off"
		}]
	}, {
		featureType : "administrative.country",
		elementType : "labels",
		stylers : [{
			color : "#000000"
		}, {
			visibility : "off"
		}]
	}];

	var map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
	map.setOptions({
		styles : styles
	});

	MAPS.map = map;

	MAPS.overlay.draw = function() {
	};
	MAPS.overlay.setMap(MAPS.map);
}

MAPS.addMarker = function addMarker(location) {

	var outer = new google.maps.Marker({
		position : location,
		clickable : false,
		icon : {
			path : google.maps.SymbolPath.CIRCLE,
			fillOpacity : 1.0,
			fillColor : '#ffff00',
			strokeOpacity : 1.0,
			strokeColor : '#000000',
			strokeWeight : 1.0,
			scale : 0,
		},
		optimized : false,
		map : MAPS.map
	});

	for (var i = 0; i <= 10; i++) {
		setTimeout(setScale(outer, i / 10), i * 150);
	}
}
function setScale(outer, scale) {
	return function() {
		if (scale == 1) {
			outer.setMap(null);
		} else {
			var icono = outer.get('icon');
			icono.strokeOpacity = Math.cos((Math.PI / 2) * scale);
			icono.fillOpacity = icono.strokeOpacity * 0.5;
			icono.scale = Math.sin((Math.PI / 2) * scale) * 20;
			outer.set('icon', icono);
		}
	}
}

MAPS.getPlaceFromLatLong = function(latitude, longitude) {

	var geocoder = new google.maps.Geocoder();

	var location = new google.maps.LatLng(latitude, longitude);

	geocoder.geocode({
		'latLng' : location
	}, function(results, status) {
		if (status == google.maps.GeocoderStatus.OK) {
			var addrComp = results[0].address_components;
			//console.log(addrComp);
			
			var state, country;
			
			jQuery.each(results[0].address_components, function(index, element) {
				
				if ($.inArray("country", element.types) >= 0) {
					country = element.long_name;
				}
				
				if ($.inArray("administrative_area_level_1", element.types) >= 0) {
					state = element.long_name;
				}
			});
			
			console.log(state + ", " + country);
			
			
		} else {
			console.log("Geocoder failed due to: " + status);
		}
	});

};

