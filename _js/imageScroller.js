(function($) {
	
	

	var defaultOptions = {

	};

	var methods = {
		init : function(options) {
			//console.log("Calling init with " + options + " and " + this.attr('id'));

			this.css('overflow', 'hidden');

			var $imageHolder = $('<div id="imageScroller"></div>').appendTo(this);

			$imageHolder.css({
				width : '0px',
				height : '320px',
				position : 'relative',
				top : '0px',
				left : '0px'
			});

		},

		addImage : function(photo) {

			var imageSrc = photo.url_z;
			var imageHeight = photo.height_z;
			var imageWidth = photo.width_z;
			var imageLat = photo.latitude;
			var imageLon = photo.longitude;
			var imageLastUpdate = photo.lastupdate;

			$scroller = $('#imageScroller');
			$content = $('#content');

			// Increasing the div size calculation
			var myImageWidth = Math.ceil(((imageWidth / imageHeight) * 300));
			var cWidth = $scroller.width();
			var nWidth = cWidth + myImageWidth;
			$scroller.width(nWidth + 'px');

			// Last Update minutes ago calculation
			var milliseconds = (new Date).getTime();
			milliseconds = Math.ceil(milliseconds / 1000);
			var lastUpdate = parseInt(photo.lastupdate);
			var timeDiff = milliseconds - lastUpdate;
			var minutes = Math.ceil(timeDiff / 60);

			var $imageHolder = $('<div class="imageHolder" id="'+ photo.id +'"></div>').appendTo($scroller);
			var $img = $('<img class="imageScrollerImage" src="' + imageSrc + '" height="300px"></img>').appendTo($imageHolder);
			var $imageInfo = $('<div class="imageInfo">' + minutes + ' minutes ago.</div>').appendTo($imageHolder);

			var noImages = $scroller.children().length;
			$imageHolder.click(function() {
				var url = "http://flickr.com/photo.gne?id=" + $(this).attr('id');
				window.open(url, '_blank');
			});

			$imageHolder.css({
				height : '300px',
				width : myImageWidth
			});

			$imageInfo.css({
				height : '20px',
				width : myImageWidth,
				background : 'black'
			});

			if (noImages > 10) {

				var $first = $scroller.children(':first');
				var firstWidth = $first.width();
				////console.log(firstWidth);

				$first.remove();
				
				//console.log('WIDTH ' + $scroller.width());

				//$scroller.css({					
					//width : '-=' + (firstWidth)			
				//});
				
				//console.log('AFTER WIDTH ' + $scroller.width());

				////console.log("Removing first child " + $scroller.children().length);
				myImageWidth = myImageWidth - firstWidth;
			}

			if ($scroller.width() > ($content.width() + 500)) {

				if (myImageWidth > 0) {
					$scroller.animate({
						left : '-=' + myImageWidth,
					}, 2000, function() {
					});
				} else {
					$scroller.animate({
						left : '+=' + Math.abs(myImageWidth),
					}, 2000, function() {
					});
				}
			}

			

		}
	}

	$.fn.imageScroller = function(method) {

		if (methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
		} else if ( typeof method === 'object' || !method) {
			return methods.init.apply(this, arguments);
		} else {
			$.error('Method ' + method + ' does not exist on jQuery.tooltip');
		}

	};
})(jQuery);
