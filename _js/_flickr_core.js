FLICKR = {};
FLICKR.url = "http://api.flickr.com/services/rest/?";
FLICKR.api_key = "52bb65bbffb415553f8477d29863c544";
FLICKR.params = {
	"api_key": FLICKR.api_key,
    "format": "json",
    "jsoncallback": "?",
	"method": ""
};


FLICKR.attachParams = function(params) {
	var _flickr_url = FLICKR.url;
	var size = Object.size(FLICKR.params);
	
	
	$.each(params, function(key, value){
		_flickr_url += key + "=" + value;
		--size;
		
		if (size != 0) {
			_flickr_url +="&";
		} 
				
	});	
	
	
	return _flickr_url;
}

FLICKR.post = function(options, callback) {
	
	var params = FLICKR.params;
	
	$.extend(params, options, false);
	
	var url = FLICKR.attachParams(params);
	
	$.getJSON(url, function(json){
		callback(json);
		
	})
	.error(function(){
		console.log("ERROR");
		
	});
}
	
