QUEUE = {};
QUEUE.photos = [];

QUEUE.startPulling = function(queue) {

	setInterval(function() {
		//console.log("QUEUE " + queue.length);

		if (queue.length > 0) {
			var photo = queue.shift();
			$('#content').imageScroller("addImage", photo);
			var location = new google.maps.LatLng(photo.latitude, photo.longitude);
			MAPS.addMarker(location);
		}
	}, 2000);
}
