Object.size = function(obj) {
	var size = 0, key;

	for (key in obj) {
		size++;
	}

	return size;
}
function attachHandlers() {
	$('#refreshButton').click(function() {
		console.log("Triggering Refresh");

		reloadImage();
		

	});

}

function reloadImage() {
	QUEUE.photos.length = 0;
	
	$("#content").empty();
	$("#content").addClass("loading");
	
	FLICKR.services.search();
	
	$('#content').imageScroller("init", "hello");

}

function revealScroller() {
	$('#content').removeClass("loading");
}

function revealInformation() {
	$("#infoBox").fadeIn(10000);
}

function revealMaps() {
	$("#map_canvas").fadeIn(10000);
}

function revealAll() {
	revealInformation();
	revealMaps();
}